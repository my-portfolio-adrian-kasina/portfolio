import * as React from "react";
import MenuList from "../Header/Menu/index";
import Sections from "../Sections";
import { scaleRotate as Menu } from "react-burger-menu";
import styles from "./styles.scss";
import burgerIcon from "./img/burgerIcon.svg";
import crossIcon from "./img/crossIcon.svg";

class BurgerMenu extends React.Component {
  showSettings(event) {
    event.preventDefault();
  }

  render() {
    return (
      <div id="outer-container" className={styles.outerContainer}>
        <Menu
          right
          burgerButtonClassName={styles.bmBurgerButton}
          crossButtonClassName={styles.bmCrossButton}
          menuClassName={styles.bmMenu}
          overlayClassName={styles.bmOverlay}
          className={styles.menuContainer}
          pageWrapId={"page-wrap"}
          outerContainerId={"outer-container"}
          customBurgerIcon={<object type="image/svg+xml" data={burgerIcon} />}
          customCrossIcon={<object type="image/svg+xml" data={crossIcon} />}
        >
          <MenuList />
        </Menu>
        <main id="page-wrap">
          <Sections />
        </main>
      </div>
    );
  }
}

export default BurgerMenu;
