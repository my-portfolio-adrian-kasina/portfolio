import React from "react";
import styles from "./styles.scss";

const About = () => (
  <div className={styles.about}>
    <h2>About me</h2>
    <p>
      For two years I've been creating websites and learning about new web
      development technologies. I created online stores based on wordpress with
      my own desing and several business card pages. In my projects, I focus on
      the principle of mobile first, optimization, code semantics and
      availability. I am currently working in London as a freelancer, but I am
      interested in working in a company in a group of people.
    </p>
  </div>
);

export default About;
