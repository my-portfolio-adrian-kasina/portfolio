import * as React from "react";
import styles from "./styles.scss";
import gitlab from "./img/gitlab.svg";
import react from "./img/react.svg";
import gulp from "./img/gulp.svg";
import webpack from "./img/webpack.svg";
import affinity from "./img/affinity.svg";
import wordpress from "./img/wordpress.svg";
import bootstrap from "./img/bootstrap.svg";
import sass from "./img/sass.svg";
import gitlabText from "../Data/gitlab.md";
import reactText from "../Data/react.md";
import gulpText from "../Data/gulp.md";
import webpackText from "../Data/webpack.md";
import affinityText from "../Data/affinity.md";
import wordpressText from "../Data/wordpress.md";
import bootsrtapText from "../Data/bootstrap.md";
import sassText from "../Data/sass.md";
import Modal from "../Modal";

class Icons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      iconsSvg: [
        { value: gitlab, text: gitlabText, showModal: false },
        { value: react, text: reactText, showModal: false },
        { value: gulp, text: gulpText, showModal: false },
        { value: webpack, text: webpackText, showModal: false },
        { value: affinity, text: affinityText, showModal: false },
        { value: wordpress, text: wordpressText, showModal: false },
        { value: bootstrap, text: bootsrtapText, showModal: false },
        { value: sass, text: sassText, showModal: false }
      ]
    };
  }

  handleShowModal = iconName => {
    this.setState(prevState => ({
      iconsSvg: prevState.iconsSvg.map(obj =>
        Object.assign(obj, { showModal: obj.value === iconName })
      )
    }));
  };

  handleHideModal = () => {
    this.setState(event => ({
      iconsSvg: event.iconsSvg.map(obj =>
        Object.assign(obj, { showModal: false })
      )
    }));
  };

  render() {
    return (
      <div className={styles.iconsBar}>
        <ul>
          {this.state.iconsSvg.map(icon => (
            <li
              key={icon.value}
              onClick={iconName => this.handleShowModal(icon.value, iconName)}
              onMouseEnter={iconName =>
                this.handleShowModal(icon.value, iconName)
              }
              onMouseLeave={iconName =>
                this.handleHideModal(icon.value, iconName)
              }
            >
              {icon.showModal ? <Modal textSouroce={icon.text} /> : null}
              <object
                className={styles.shadow}
                type="image/svg+xml"
                data={icon.value}
              >
                Icon {icon.value}
              </object>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default Icons;
