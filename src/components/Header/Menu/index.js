import * as React from "react";
import styles from "./styles.scss";

const Menu = () => (
  <ul>
    <li>
      <a href="/">About me</a>
    </li>
    <li>
      <a href="/">Skills</a>
    </li>
    <li>
      <a href="/">Portfolio</a>
    </li>
    <li>
      <a href="/">Contact</a>
    </li>
  </ul>
);

export default Menu;
