import * as React from "react";
import styles from "./styles.scss";
import arrowDown from "../../assets/imgs/arrow.svg";
import Menu from "./Menu/index";

const Header = () => (
  <header className={styles.header}>
    <div className={styles.titleContainer}>
      <span>Hey,</span>
      <h1 className={styles.title}>
        <span>I'm</span>
        Adrian Kasina
      </h1>
      <span>Web Developer</span>
      <p>
        I am happy to work with a client or employer. I created this site in
        React to refine my knowledge and try this library. I also want to show
        my level of knowledge with a web developer using this page. Below is a
        link to the source code.
      </p>
    </div>
    <nav className={styles.buttonsNav}>
      <Menu />
    </nav>
    <img className={styles.arrowDown} src={arrowDown} alt="" />
  </header>
);

export default Header;
