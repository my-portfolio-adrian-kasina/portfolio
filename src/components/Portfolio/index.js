import * as React from "react";
import styles from "./styles.scss";
import CarouselContainer from "./Slides/index";

const Portfolio = () => (
  <section className={styles.sectionPortfolio}>
    <div className={styles.container}>
      <h2> Portfolio </h2>
      <CarouselContainer />
    </div>{" "}
  </section>
);

export default Portfolio;
