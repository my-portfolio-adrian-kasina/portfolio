import * as React from "react";
import styles from "../../styles.scss";

const Aniamtion = () => (
  <div className={styles.examplesWorks + " " + styles.bgKoduje}>
    <div className={styles.exampleDescription}>
      <h3>Animation SVG</h3>
      <p>
        First place in the competition for adding animation to a still vector
        image. This is the result of exercises and experimenting with javascript
        and SVG graphics.
      </p>
      <a
        className={styles.linkToExample}
        href="https://codepen.io/GuyDiamond/pen/rwdGBQ"
        target="_blank"
      >
        {" "}
        Show me{" "}
      </a>
    </div>
  </div>
);
export default Aniamtion;
