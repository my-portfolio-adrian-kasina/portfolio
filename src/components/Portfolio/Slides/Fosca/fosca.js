import React, { Component } from "react";
import styles from "../../styles.scss";
import gif from "./img/fosca.gif";

class ViewFosca extends Component {
  render() {
    let modalFosca = (
      <div className={styles.foscaGif}>
        <img
          className={styles.presentationFosca}
          src={gif}
          alt="Online Store with WooCommerce & WordPress"
        />
        <button
          className={styles.closeButtonModal}
          onClick={this.props.isClose}
        >
          x
        </button>
      </div>
    );

    if (!this.props.isOpen) {
      modalFosca = null;
    }

    return <div>{modalFosca}</div>;
  }
}

export default ViewFosca;
