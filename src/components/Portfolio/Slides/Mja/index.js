import * as React from "react";
import styles from "../../styles.scss";

const Mja = () => (
  <div className={styles.examplesWorks + " " + styles.bgMja}>
    <div className={styles.exampleDescription}>
      <h3>Bussines website</h3>
      <p>
        My last order. The owner was keen on short execution time. I made two
        graphic designs of the website, several logo designs, the appearance of
        a business card and implemented the website. The website achieves very
        good results at the Lighthouse report: Performace - 99, Accessbility -
        92, SEO - 96.
      </p>
      <a
        className={styles.linkToExample}
        href="http://www.mjasurrey.com"
        target="_blank"
      >
        {" "}
        Show me{" "}
      </a>
    </div>
  </div>
);
export default Mja;
