import * as React from "react";
import styles from "../../styles.scss";

const Game = () => (
  <div className={styles.examplesWorks + " " + styles.bgGame}>
    <div className={styles.exampleDescription}>
      <h3> Game for kids in javascript </h3>
      <p>
        I did the game to practice javascript and interaction of SVG graphics
        with HTML. Game was supposed to help my daughter learn letters.
      </p>
      <a
        className={styles.linkToExample}
        href="https://gitlab.com/adminet/where_is_my_letter"
        target="_blank"
      >
        {" "}
        Show me{" "}
      </a>
    </div>
  </div>
);
export default Game;
