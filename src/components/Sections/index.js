import * as React from "react";
import Header from "../Header";
import About from "../About";
import Skills from "../Skills";
import Portfolio from "../Portfolio";
import Contact from "../Contact";
import styles from "./sections.scss";

const Sections = () => (
  <div className={styles.container}>
    <Header />
    <About />
    <Skills />
    <Portfolio />
    <Contact />
  </div>
);

export default Sections;
