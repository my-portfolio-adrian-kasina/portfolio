import * as React from "react";
import MetaHead from "../../metatags";
import styles from "./styles.scss";
import BurgerMenu from "../Burgermenu";

const App = () => (
  <div className={styles.mainContainer}>
    <MetaHead />
    <BurgerMenu />
  </div>
);

export default App;
