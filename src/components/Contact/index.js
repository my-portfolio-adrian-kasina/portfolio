import * as React from "react";
import styles from "./styles.scss";

class contactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      subject: "",
      message: ""
    };
  }

  handleTextInputChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  //   handleSubit = e => {
  //     alert(
  //       "Wyslane dane: " +
  //         this.state.username +
  //         ", " +
  //         this.state.email +
  //         ", " +
  //         this.state.subject +
  //         ", " +
  //         this.state.message
  //     );
  //     e.preventDefalult();
  //   };

  render() {
    return (
      <section className={styles.sectionContact}>
        <div className={styles.titleContact}>
          <h2>Contact</h2>
          <a href="mailto:kasina.adrian@gmail.com">kasina.adrian@gmail.com</a>
        </div>
        <form onSubmit={this.handleSubit} className={styles.contactForm}>
          <label htmlFor="username">Name</label>
          <input
            value={this.state.username}
            onChange={this.handleTextInputChange}
            type="text"
            id="username"
            name="username"
          />

          <label htmlFor="email">Address email</label>
          <input
            value={this.state.email}
            onChange={this.handleTextInputChange}
            type="text"
            id="email"
            name="email"
          />

          <label htmlFor="subject">Subject</label>
          <input
            value={this.state.subject}
            onChange={this.handleTextInputChange}
            type="text"
            id="subject"
            name="subject"
          />

          <label htmlFor="message">Message</label>
          <textarea
            value={this.state.message}
            onChange={this.handleTextInputChange}
            type="text"
            id="message"
            name="message"
          />

          <button type="subit">Send</button>
        </form>
      </section>
    );
  }
}

export default contactForm;
